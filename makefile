CPP = gcc
EXE = C8Emulator

SRC_DIR = src
OBJ_DIR = build

SRC = $(wildcard $(SRC_DIR)/*.c)
OBJ = $(SRC:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)

CPPFLAGS += -I.
CFLAGS += -Wall
LDFLAGS += -lmingw32 -lSDL2main -lSDL2 -I E:\Programs\SDL2-MinGW\include
LDLIBS += -L E:\Programs\SDL2_MinGW\lib

.PHONY: all clean

all: $(EXE)

$(EXE): $(OBJ)
	$(CPP) $^ -o $@ $(LDLIBS) $(LDFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CPP) $(CPPFLAGS) $(CFLAGS) -c $< -o $@
