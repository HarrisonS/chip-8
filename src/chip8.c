#include "chip8.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

Chip8* chip8_init() {
    Chip8* chip8;
    chip8 = malloc(sizeof(Chip8));

    srand(time(0));

    chip8_reset(chip8);

    return chip8;
}

void chip8_reset(Chip8* chip8) {
    chip8->pc = 0x200;
    chip8->sp = 0;
    chip8->I = 0;
    chip8->delay_timer = 0;
    chip8->sound_timer = 0;
    chip8->draw_flag = true;

    memset(chip8->V, 0, sizeof(chip8->V));
    for (int i = 0; i < 80; i++) chip8->memory[i] = fontset[i];
    memset(chip8->memory+80, 0, sizeof(chip8->memory));
    memset(chip8->stack, 0, sizeof(chip8->stack));
    memset(chip8->display, 0, sizeof(chip8->display));
    memset(chip8->keys, false, sizeof(chip8->keys));
}

void chip8_load_program(Chip8* chip8, const char* filepath) {
    FILE* fp;
    fp = fopen(filepath, "rb");
    if (!fp) {
        printf("Unable to load program");
        exit(1);
    }

    fseek(fp, 0L, SEEK_END);
    int size = ftell(fp);
    fseek(fp, 0L, SEEK_SET);

    uint8_t *buffer = &chip8->memory[512];
    fread(buffer, size, 1, fp);
}

static void unimplemented_instruction(uint16_t opcode) {
    printf("Unimplemented opcode: %x", opcode);
    exit(1);
}

#define VX(opcode) chip8->V[(opcode & 0x0F00) >> 8]
#define VY(opcode) chip8->V[(opcode & 0x00F0) >> 4]
void chip8_emulate_cycle(Chip8* chip8) {
    uint16_t opcode = chip8->memory[chip8->pc] << 8 | chip8->memory[chip8->pc+1];

    /* For debugging purposes
    printf("%x - %x -", chip8->pc, opcode);
    for (int i = 0; i < 16; i++) {
        printf(" %x:%x", i, chip8->V[i]);
    }
    printf(" I:%x\n", chip8->I);
    */

    switch (opcode & 0xF000) {
        case 0x0000:
            switch (opcode & 0x000F) {
                case 0x0000: // Display clear
                    unimplemented_instruction(opcode);
                    break;
                case 0x000E: // Return
                    chip8->pc = chip8->stack[chip8->sp];
                    chip8->sp--;
                    break;
                default:
                    printf("Error, invalid 0x0 opcode: %x", opcode);
                    break;
            }
            break;
        case 0x1000: // 0x1NNN - Jump to NNN
            chip8->pc = (opcode & 0x0FFF);
            break;
        case 0x2000: // 0x2NNN - Call NNN
            chip8->sp++;
            chip8->stack[chip8->sp] = chip8->pc + 2;
            chip8->pc = (opcode & 0x0FFF);
            break;
        case 0x3000: // 0x3XNN - skip if VX==NN
            if (VX(opcode) == (opcode & 0x00FF)) {
                chip8->pc += 2;
            }
            chip8->pc += 2;
            break;
        case 0x4000: // 0x4XNN - skip if VX!=NN
            if (VX(opcode) != (opcode & 0x00FF)) {
                chip8->pc += 2;
            }
            chip8->pc += 2;
            break;
        case 0x5000: // 0x5XY0 - skip if VX==VY
            if (VX(opcode) == VY(opcode)) {
                chip8->pc += 2;
            }
            chip8->pc += 2;
            break;
        case 0x6000: // 0x6XNN - VX = NN
            VX(opcode) = (opcode & 0x00FF);
            chip8->pc += 2;
            break;
        case 0x7000: // 0x7XNN - VX += NN
            VX(opcode) += (opcode & 0x00FF);
            chip8->pc += 2;
            break;
        case 0x8000: // Arithmetic
            switch (opcode & 0x000F) {
                case 0x0000: // 0x8XY0 - VX = VY
                    VX(opcode) = VY(opcode);
                    chip8->pc += 2;
                    break;
                case 0x0001: // 0x8XY1 - VX = VX | VY
                    VX(opcode) = VX(opcode) | VY(opcode);
                    chip8->pc += 2;
                    break;
                case 0x0002: // 0x8XY2 - VX = VX & VY
                    VX(opcode) = VX(opcode) & VY(opcode);
                    chip8->pc += 2;
                    break;
                case 0x0003: // 0x8XY3 - VX = VX ^ VY
                    VX(opcode) = VX(opcode) ^ VY(opcode);
                    chip8->pc += 2;
                    break;
                case 0x0004: // VX += VY, VF = 1 if carry
                    chip8->V[0xF] = (VY(opcode) > (0xFF - VX(opcode))) ? 1 : 0;
                    VX(opcode) += VY(opcode);
                    chip8->pc += 2;
                    break;
                case 0x0005: // 0x8XY5 - VX -= VY, VF = 0 if borrow
                    chip8->V[0xF] = (VY(opcode) > VX(opcode)) ? 0 : 1;
                    VX(opcode) -= VY(opcode);
                    chip8->pc += 2;
                    break;
                case 0x0006: // 0x8XY6 - VF = least sig bit of VX, VX shifted right 1
                    chip8->V[0xF] =  (VX(opcode) & 0x0001);
                    VX(opcode) >>= 1;
                    chip8->pc += 2;
                    break;
                case 0x0007: // 0x8XY7 - VX = VY - VX, VF = 0 if borrow
                    chip8->V[0xF] = (VX(opcode) > VY(opcode)) ? 0 : 1;
                    VX(opcode) = VY(opcode) - VX(opcode);
                    chip8->pc += 2;
                    break;
                case 0x000E: // 0x8XYE - VF = most sig bit of VX, VX shifted left 1
                    chip8->V[0xF] = (VX(opcode) & 0x0080);
                    VX(opcode) <<= 1;
                    chip8->pc += 2;
                    break;
                default:
                    printf("Error, invalid 0x8 opcode: %x", opcode);
                    exit(1);
                    break;
            }
            break;
        case 0x9000: // 0x9XY0 - Skip if VX != VY
            if (VX(opcode) != VY(opcode)) {
                chip8->pc += 2;
            }
            chip8->pc += 2;
            break;
        case 0xA000: // 0xANNN - I = NNN
            chip8->I = (opcode & 0x0FFF);
            chip8->pc += 2;
            break;
        case 0xB000: // 0xBNNN - Jump NNN + V0
            chip8->pc = (opcode & 0x0FFF) + chip8->V[0];
            break;
        case 0xC000: // 0xCXNN - VX = rand & NN
            VX(opcode) = (rand() & 256) & (opcode & 0x00FF);
            chip8->pc += 2;
            break;
        case 0xD000: { // 0xDXYN - Draw at VX, VY with height N
            uint16_t pixel;
            chip8->V[0xF] = 0;

            for (int y = 0; y < (opcode & 0x000F); y++) {
                pixel = chip8->memory[chip8->I + y];
                for (int x = 0; x < 8; x++) {
                    if ((pixel & (0x80 >> x)) != 0) {
                        if (chip8->display[VX(opcode) + x + ((VY(opcode) + y) * 64)] == 1) {
                            chip8->V[0xF] = 1;
                        }
                        chip8->display[VX(opcode) + x + ((VY(opcode) + y) * 64)] ^= 1;
                    }
                }
            }

            chip8->draw_flag = true;
            chip8->pc += 2;
            break;
        } case 0xE000:
            switch (opcode & 0x000F) {
                case 0x0001: // 0xEXA1 - Skip if key in VX isn't pressed
                    if (!chip8->keys[VX(opcode)]) {
                        chip8->pc += 2;
                    }
                    chip8->pc += 2;
                    break;
                case 0x000E: // 0xEX9E - Skip if key in VX is pressed
                    if (chip8->keys[VX(opcode)]) {
                        chip8->pc += 2;
                    }
                    chip8->pc += 2;
                    break;
                default:
                    printf("Error, invlaid 0xE opcode: %x", opcode);
                    exit(1);
                    break;
            }
            break;
        case 0xF000:
            switch (opcode & 0x00FF) {
                case 0x0007: // 0xFX07 - VX = delay timer
                    VX(opcode) = chip8->delay_timer;
                    chip8->pc += 2;
                    break;
                case 0x000A: // 0xFX0A - Key press awaited then stored in VX
                    unimplemented_instruction(opcode);
                    break;
                case 0x0015: // 0xFX15 - delay timer = VX
                    chip8->delay_timer = VX(opcode);
                    chip8->pc += 2;
                    break;
                case 0x0018: // 0xFX18 - sound timer = VX
                    chip8->sound_timer = VX(opcode);
                    chip8->pc += 2;
                    break;
                case 0x001E: // 0xFX1E - I += VX, VF = 1 if range overflow
                    chip8->V[0xF] = ((chip8->I + VX(opcode)) > 0xFFF) ? 1 : 0;
                    chip8->I += VX(opcode);
                    chip8->pc += 2;
                    break;
                case 0x0029: // 0xFX29 - I = sprite for char in VX
                    chip8->I = chip8->V[VX(opcode) * 5]; // address of char in fontset = char * 5
                    chip8->pc += 2;
                    break;
                case 0x0033: { // 0xFX33 - BCD
                    int bcd = VX(opcode);
                    chip8->memory[chip8->I] = bcd / 100;
                    chip8->memory[chip8->I + 1] = (bcd % 100) / 10;
                    chip8->memory[chip8->I + 2] = (bcd % 10);
                    chip8->pc += 2;
                    break;
                } case 0x0055: // 0xFX55 - V0-VX stored in memory at I
                    for (int i = 0; i <= VX(opcode); i++) {
                        chip8->memory[chip8->I + i] = chip8->V[i];
                    }
                    chip8->pc += 2;
                    break;
                case 0x0065: // 0xFX65 - V0-VX filled from memory at I
                    for (int i = 0; i <= VX(opcode); i++) {
                        chip8->V[i] = chip8->memory[chip8->I + i];
                    }
                    chip8->pc += 2;
                    break;
                default:
                    printf("Error invalid 0xF opcode: %x", opcode);
                    exit(1);
                    break;
            }
            break;
        default:
            printf("Error, invalid opcode: %x", opcode);
            exit(1);
            break;
    }

    if (chip8->delay_timer > 0) {
        chip8->delay_timer--;
    }

    if (chip8->sound_timer > 0) { // TODO: implement sound
        chip8->sound_timer--;
    }
}
