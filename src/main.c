#include <stdio.h>

#include "chip8.h"
#include "display.h"
#include "keypad.h"
#undef main

int main(void) {
    // init graphics and input
    Display* display = display_init();
    keypad_init();
    
    // init chip8
    Chip8* chip8 = chip8_init();
    chip8_load_program(chip8, "breakout"); // TODO: change to be based on user input

    while (!display->quit) {
        chip8_emulate_cycle(chip8);

        // check input
        keypad_update_keys(chip8->keys);

        display_update(display);
        if (chip8->draw_flag) {
            display_draw_buffer(display, chip8->display, 64, 32);
            chip8->draw_flag = false;
        }
    }

    display_free(display);
    keypad_free();

    return 0;
}
