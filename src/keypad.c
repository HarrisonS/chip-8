#include "keypad.h"

#include <stdlib.h>

void keypad_init() {
    SDL_Init(SDL_INIT_EVENTS);
}

void keypad_free() {
    SDL_Quit();
}

void keypad_update_keys(bool* keys) {
    SDL_Event event;
    SDL_PollEvent(&event);

    switch (event.type) {
       	case SDL_KEYDOWN:
			switch (event.key.keysym.sym) {
				case SDLK_1:
					keys[1] = true;
					break;
				case SDLK_2:
					keys[2] = true;
					break;
				case SDLK_3:
					keys[3] = true;
					break;
				case SDLK_4:
					keys[12] = true;
					break;
				case SDLK_q:
					keys[4] = true;
					break;
				case SDLK_w:
					keys[5] = true;
					break;
				case SDLK_e:
					keys[6] = true;
					break;
				case SDLK_r:
					keys[13] = true;
					break;
				case SDLK_a:
					keys[7] = true;
					break;
				case SDLK_s:
					keys[8] = true;
					break;
				case SDLK_d:
					keys[9] = true;
					break;
				case SDLK_f:
					keys[14] = true;
					break;
				case SDLK_z:
					keys[10] = true;
					break;
				case SDLK_x:
					keys[0] = true;
					break;
				case SDLK_c:
					keys[11] = true;
					break;
				case SDLK_v:
					keys[15] = true;
					break;
				default:
					break;
			}
			break;
		case SDL_KEYUP:
			switch (event.key.keysym.sym) {
				case SDLK_1:
					keys[1] = false;
					break;
				case SDLK_2:
					keys[2] = false;
					break;
				case SDLK_3:
					keys[3] = false;
					break;
				case SDLK_4:
					keys[12] = false;
					break;
				case SDLK_q:
					keys[4] = false;
					break;
				case SDLK_w:
					keys[5] = false;
					break;
				case SDLK_e:
					keys[6] = false;
					break;
				case SDLK_r:
					keys[13] = false;
					break;
				case SDLK_a:
					keys[7] = false;
					break;
				case SDLK_s:
					keys[8] = false;
					break;
				case SDLK_d:
					keys[9] = false;
					break;
				case SDLK_f:
					keys[14] = false;
					break;
				case SDLK_z:
					keys[10] = false;
					break;
				case SDLK_x:
					keys[0] = false;
					break;
				case SDLK_c:
					keys[11] = false;
					break;
				case SDLK_v:
					keys[15] = false;
					break;
				default:
					break;
			}
			break;
		default:
			break;
    }
}
