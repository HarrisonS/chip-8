#ifndef DISPLAY_H
#define DISPLAY_H

#include <SDL2/SDL.h>
#include <stdbool.h>

typedef struct Display {
    SDL_Window* window;
    SDL_Renderer* renderer;
    SDL_Texture* texture;
    SDL_Event event;
    uint32_t pixels[320 * 640];
    bool quit;
} Display;

Display* display_init();

void display_free(Display* display);

void display_update(Display* display);

void display_draw_buffer(Display* display, uint8_t* buffer, int w, int h);

#endif
