#include "display.h"

#include <stdlib.h>
#include <stdio.h>

const int HEIGHT = 320, WIDTH = 640;

Display* display_init() {
    Display* display = malloc(sizeof(Display));

    SDL_Init(SDL_INIT_VIDEO);
    display->window = SDL_CreateWindow("Chip8 Emulator", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_ALLOW_HIGHDPI);
    display->renderer = SDL_CreateRenderer(display->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    display->texture = SDL_CreateTexture(display->renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, WIDTH, HEIGHT);
    for (int i = 0; i < WIDTH * HEIGHT; i++) display->pixels[i] = 0;

    display->quit = false;

    return display;
}

void display_free(Display* display) {
    SDL_DestroyTexture(display->texture);
    SDL_DestroyRenderer(display->renderer);
    SDL_DestroyWindow(display->window);
    SDL_Quit();
}

void display_update(Display* display) {
    if (SDL_PollEvent(&display->event)) {
        switch (display->event.type) {
            case SDL_QUIT:
                display->quit = true;
                break;
        }
    }
}

void display_draw_buffer(Display* display, uint8_t* buffer, int w, int h) {
    int sw = WIDTH / w;
    int sh = HEIGHT / h;
    for (int y = 0; y < HEIGHT; y++) {
        for (int x = 0; x < WIDTH; x++) {
            display->pixels[(y * WIDTH) + x] = buffer[(y / sh) * w + (x / sw)] * 0xFFFFFF;
        }
    }

    SDL_UpdateTexture(display->texture, NULL, display->pixels, WIDTH * sizeof(uint32_t));
    SDL_RenderClear(display->renderer);
    SDL_RenderCopy(display->renderer, display->texture, NULL, NULL);
    SDL_RenderPresent(display->renderer);
}
