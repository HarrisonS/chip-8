#ifndef KEYPAD_H
#define KEYPAD_H

#include <SDL2/SDL.h>
#include <stdint.h>
#include <stdbool.h>

void keypad_init();

void keypad_free();

void keypad_update_keys(bool* keys);

#endif
